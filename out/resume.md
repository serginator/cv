Sergio Ruiz
============
Email: serginator@gmail.com

Web: http://www.serginator.com

**FullStack JavaScript developer with 5+ years industry experience, since the age of 6 with computers and since the age of 12 dealing with code**, with strong skills in code quality, performance and continuous integration. Capacity to learn new languages and adaptability to new environments in no time thanks to his overall knowledge of languages and operative systems.

## SKILLS

  - Web: HTML5 CSS3 JavaScript REST AJAX 
  - JavaScript: jQuery AngularJS Node.js Zepto.js Underscore 
  - CI: Jenkins Sonar Grunt.js Unix Docker 
  - Databases: MySQL SQLite NoSQL MongoDB 
  - SCM: Git Github SVN TFS 

## EMPLOYMENT

### *Analyst Programmer*, [AURIGAE](http://www.aurigae.com/) (2012-12 — Present)

Software Developer at Telefónica I+D. I've worked mainly focused in full-stack javascript, continuous integration and evangelist of best practices, most of the time at video area.

### *Systems Integration Consultant - Programmer*, [CIBER](http://www.ciber.com/es) (2012-01 — 2012-12)

Programmer at System Integration, I've been developing a social network, Android apps, rest services at BBVA GAE project, sites with Google Sites for internal documentation at BBVA and Telepizza's mobile app.

### *Systems Integration Consultant - Junior Programmer*, [CIBER](http://www.ciber.com/es) (2011-06 — 2011-12)

Junior Programmer at System Integration developing a social network.


## PROJECTS

### *Creator*, [JS Test Lab](http://www.serginator.com/) (2011-03 — Present)

A website I made as a portfolio for a Japanese company I was going to work for before the tsunami in 2011.
An arcade machine

### *Developer*, [Movistar TV - DTH Hybrid](http://www.movistar.com) (2015-01 — Present)

A set-top box (STB) with satellital and internet connection for TVApps and VOD
Development of the UI of a set-top box (STB) with satellital and internet connection for TVApps and VOD, using SVG and JavaScript to communicate with the middleware. Also I've installed and configured all the continuous integration environment with Jenkins and Sonar, and made the Grunt.js tasks to automate everything, from linting or testing to opening pull requests automagically integrating Crowdin with new translations from the OBs.

### *Developer*, [Movistar TV - GO](http://www.movistar.com) (2015-02 — 2015-03)

Movistar TV web application, which allows to watch Live TV and VOD content, and handle your subscriptions, view the EPG and more.
Movistar TV web application, which allows to watch Live TV and VOD content, and handle your subscriptions, view the EPG and more. Joined the team for one month to speed development of new features.

### *Main developer*, [Movistar TV - Chromecast Receiver](http://www.movistar.com) (2015-01 — Present)

Chromecast Receiver for the mobile applications of Movistar TV, which handles to play Live TV and VOD on Chromecast.
Chromecast Receiver for the mobile applications of Movistar TV, which handles to play Live TV and VOD on Chromecast. Developed in Angular JS. Also installed and configured all the continuous integration environment with Jenkins and Sonar, and made the Grunt.js tasks to automate everything, from linting or testing to opening pull requests automagically integrating Crowdin with new translations from the OBs.

### *Main developer*, [CSB - Cloud Service Broker](http://www.o2.co.uk/business/products-and-services/business-apps/mcafee-multi-access) (2014-01 — 2014-11)

CSB is a panel to manage users, assign resources or services, handle your account as a user... It uses Parallels (POA and PBA).
CSB is a panel to manage users, assign resources or services, handle your account as a user... It uses Parallels (POA and PBA). Development of an APS 1.2 to integrate McAfee Multi Access in CSB, front-end (in Dojo Toolkit) and back-end (in Node.js), for O2 (UK and GER) and VIVO (Brazil). Also made all the ci environment.

### *Developer*, FeedApp (2013-07 — 2014-02)

Web application connected to internal LDAP server which allows to send feedback to people personally or anonymously about projects, behaviour...
Web application made with the magic of Meteor.js and Lungo.js, connected to our internal LDAP server to handle logins. After finishing it, we ported all the Lungo.js part to Firefox building blocks as some of the members of the team were members of Firefox OS and wanted to use it in their devices. This app allows to send feedback to people personally or anonymously about projects, behaviour...

### *Developer*, [Movistar TV - DTH Hybrid](http://www.movistar.com) (2012-12 — 2013-12)

A set-top box (STB) with satellital and internet connection for TVApps and VOD
Development of the UI of a set-top box (STB) with satellital and internet connection for TVApps and VOD, using SVG and JavaScript to communicate with the middleware. Also I've installed and configured all the continuous integration environment with Jenkins and Sonar, and made the Grunt.js tasks to automate everything, from linting or testing to opening pull requests automagically integrating Crowdin with new translations from the OBs.

### *Developer*, [Telepizza - Mobile app development](http://www.telepizza.es) (2012-09 — 2012-12)

Web application which ended being wrapped in phonegap to be the mobile application for Telepizza.
Development of the Smartphone/Tablet application, with our own javascript framework to load all sections dynamically in a single page application.

### *Developer*, [BBVA - Google App Engine (GAE) Team](http://tecnologia.elpais.com/tecnologia/2012/01/11/actualidad/1326276065_850215.html) (2012-06 — 2012-09)

Use of Google Apps inside the company to handle users, data, documents...
Development of services and apps for BBVA using Google App Engine (GAE). After the development of several REST services, I created some documentation webs for internal projects using Google Sites and developing gadgets in Google Apps Script.

### *Main developer*, [HEEL - Android application](http://www.heel.es) (2012-02 — 2012-02)

A Tablet application in Android for a pharmaceutical laboratory.
Development of a tablet application (Android) for a pharmaceutical laboratory. This app is used to let pharmacists to fill and sign contracts of sale and purchase, with a pharmacy database. It captures the form as a digital image and stores the filled form in CVS format to post-process it.

### *Developer*, [TUYYOU - Development of a Social Network](http://www.tuyyou.com) (2011-06 — 2012-06)

Social network which pretends to join a buying platform with a professional network, offering the possibility to earn money from your referrals.
Social network development using ELGG and modifying it's core. It was running for several years, being announced in several media around the country.



## EDUCATION

### [Imagina Formación](http://www.imaginaformacion.com) (2015-12 — 2016-02)

Angular JS course


### [Rice University](https://www.coursera.org) (2013-01 — 2013-01)

A course by Rice University at Coursera, Graded with distinction: https://www.coursera.org/records/XKhb4VhWeYRPvHnc


### [Universidad Nacional de Educación a Distancia - UNED](https://en.wikipedia.org/wiki/Medfield_College) (2011-01 — 2012-01)

Android development


### [Universidad Complutense de Madrid - UCM](https://www.ucm.es) (2010-01 — 2010-01)

Course certified by UCM about HTML4, CSS2 and Dreamweaver.


### [Universidad Complutense de Madrid - UCM](https://www.ucm.es) (2005-10 — 2010-12)

Studied Engineering in Computer Science until 3rd grade, passed all programming labs.



## AFFILIATION

### *Co-Organizer and member*, [MadridJS Meetup](http://www.meetup.com/es/madridjs/) (2011-08 — Present)

Co-Organizer of MadridJS, helping with talks and organization but with the lack of time, just member since 2015

### *Member*, [HTML5 Spain Meetup](http://www.meetup.com/es/HTML5-Spain/) (2012-02 — Present)

Member of HTML5 Spain

### *Member*, [JavaScript study group](http://gejs.jottit.com/) (2011-08 — 2011-09)

Study group formed by several members of the JavaScript community in Madrid to learn and debate about JavaScript ecosystem.







## SPEAKING

### *Game development in JavaScript*, MadridJS Meetup (2012)

It was a conference [Carlos](http://twitter.com/etnassoft) and I did about Game Development in JavaScript. It was in CAMON, with around 90 attendees. Here is the [video](https://vimeo.com/39259983), [slides](http://www.serginator.com/juegos-en-js/#/home) and [info about the session](http://lanyrd.com/2012/madridjs/sdqxgc/).

### *Workshop: Game programming in JavaScript*, GameMe5 (2013)

Advanced workshop on how to develop a game without libraries, plain JavaScript, for the [GameMe5 event](http://html5-spain.com/html5-spain/GameMe5/index.html). In this workshop we showed and explained in four hours, step by step, a shootem up demo. The game can be played [here](http://serginator.github.io/workshopGameMe5/) and the repo with more info can be watched [here](https://github.com/serginator/workshopGameMe5).



## INTERESTS

- TRAVELLING
Culture and landscapes everywhere!

- READING
I love to read, books, comics or manga. Action, Sci-Fi, Fantastic, Horror...

- WATCHING MOVIES AND TV SHOWS
I like movies and tv shows. Action, Sci-Fi, Fantastic, Horror...

- VIDEO GAMES
I love to play video games, PC, consoles, handhelds... And make them!

- MUSIC LOVER
I think life without music has no meaning, so I try to listen music every time I can. Rock, blues, jazz, hard rock, heavy metal, classical music...

- GUITAR PLAYING
I've played guitar since 2003 during 8 years, currently on hold due to lack of time.

- MAGIC TRICKS
Studied and practice card tricks for several years, currently on hold due to lack of time.

- JAPANESE
Studied Japanese for two years (2009, 2010), levels A1.1 and A1.2 at CSIM.


